public class Monkey{
	private String breed;
	private String currentMood;
	private int age;
	
	public Monkey(String breed, String currentMood, int age) {
		this.breed = breed;
		this.currentMood = currentMood;
		this.age = age;
	}
	
	public String getBreed() {
		return this.breed;
	}
	
	public String getCurrentMood() {
		return this.currentMood;
	}
	
	public int getAge() {
		return this.age;
	}
	
	public void setCurrentMood(String mood) {
		this.currentMood = mood;
	}
	
	public void eatBananas() {
		if(this.currentMood.equals("Hungry")){
			System.out.println("Please feed your Monkey bananas!! Otherwise it will explode on you.");
		}
		else{
			System.out.println("Five banana per day for your Monkey keeps his hunger away");
		}
	}
	public void sayAge(){
		if(this.breed.equals("Hamadryas")){
			System.out.println("The females groom the dominant male more often than the other females.");
		}
		else if(this.breed.equals("Guinea")){
			System.out.println("Guinea baboons are found in grassy, rocky, and steppe habitats in western Africa.");
		}
		else if(this.breed.equals("Olive")){
			System.out.println("Olive baboons are polygynandrous (promiscuous) meaning both the males and females have multiple partners.");
		}
		else if(this.breed.equals("Yellow")){
			System.out.println("It is yellow. LOL");
		}
		else{
			System.out.println("I dont have a fun fact for you, since this program only works for Baboons");
		}
	}
}