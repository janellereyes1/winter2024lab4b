import java.util.Scanner;

public class VirtualPetApp{
	public static void main(String[] args){
		
		Scanner reader = new Scanner (System.in);
		
		Monkey[] baboons = new Monkey[1];
		
		for(int i=0; i < baboons.length; i++){
			
			System.out.println("What breed is your Baboon?");
			//baboons[i].setBreed(reader.nextLine());
			String breed = reader.nextLine();
			
			System.out.println("What is your Baboons current mood?");
			//baboons[i].setCurrentMood(reader.nextLine());
			String currentMood = reader.nextLine();
			
			System.out.println("How old is your Baboon?");
			//baboons[i].setAge(Integer.parseInt(reader.nextLine()));
			int age = Integer.parseInt(reader.nextLine());
			
			baboons[i] = new Monkey(breed, currentMood, age);
		}
		System.out.println("What is ur Baboons new mood?");
		String newMood = reader.nextLine();
		baboons[baboons.length-1].setCurrentMood(newMood);
		
		System.out.println("");
		
		System.out.println(baboons[baboons.length-1].getBreed());
		System.out.println(baboons[baboons.length-1].getCurrentMood());
		System.out.println(baboons[baboons.length-1].getAge());
		
		//baboons[0].eatBananas();
		//baboons[0].sayAge();
	}
}